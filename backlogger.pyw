#!/usr/bin/python

import os
import sys
import math
import time
import json
from PyQt5.QtCore import Qt, QDateTime, pyqtSlot, qFatal
from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import *

DATABASE_FILE = "db"

def currentDateTimeString():
	return QDateTime.currentDateTime().toString(Qt.ISODate).replace('T', ' ')

def durationString(a, b):
	duration = a.secsTo(b)
	res = []
	for label, divisor in zip(["yrs", "d", "h"], [365.24*24*60*60, 24*60*60, 60*60]):
		quotient, remainder = duration/divisor, math.fmod(duration, divisor)
		res.append({'amount': int(quotient), 'label': label})
		duration = remainder
	while len(res) > 1 and res[0]['amount'] == 0: # Remove leading zeroes.
		res.pop(0)
	return ", ".join([str(item['amount']) + item['label'] for item in res])

# This is the context menu within the main view.
class BacklogMenu(QMenu):
	def __init__ (self, parent):
		super().__init__(parent)
		self.actionCallbacks = {}
		action = QAction('Add', self)
		action.setShortcut(QKeySequence(Qt.CTRL + Qt.Key_Insert))
		self.addAction(action)
		self.actionCallbacks[action] = self.add
		if self.parentWidget().hasSelection():
			labels = ['Last Played Now', 'Delete', 'Increment Priority', 'Decrement Priority']
			shortcuts = [Qt.CTRL + key for key in [Qt.Key_Space, Qt.Key_Delete, Qt.Key_Plus, Qt.Key_Minus]]
			callbacks = [self.bump, self.delete, self.priorityUp, self.priorityDown]
			for label, shortcut, callback in zip(labels, shortcuts, callbacks):
				action = QAction(label, self)
				action.setShortcut(QKeySequence(shortcut))
				self.addAction(action)
				self.actionCallbacks[action] = callback

	def showMenu(self, pos):
		action = self.exec_(self.parentWidget().mapToGlobal(pos))
		if action:
			self.actionCallbacks[action]()

	def add(self):
		self.parentWidget().add()

	def bump(self):
		self.parentWidget().bump()

	def delete(self):
		self.parentWidget().delete()

	def priorityUp(self):
		self.parentWidget().modifyPriority(lambda x: x + 1)

	def priorityDown(self):
		self.parentWidget().modifyPriority(lambda x: x - 1)

# Represents a cell in the main view.
class BacklogListItem(QTableWidgetItem):
	IntType = QTableWidgetItem.UserType
	StringType = QTableWidgetItem.UserType + 1
	DateType = QTableWidgetItem.UserType + 2
	DurationType = QTableWidgetItem.UserType + 3

	def __init__(self, text, type = StringType):
		super().__init__(text, type)
		self.setText(text)

	def setData(self, role, value):
		if role == Qt.DisplayRole:
			if self.type() == self.IntType:
				if self.validate(value):
					super().setData(role, value)
				self.setTextAlignment(Qt.AlignRight | Qt.AlignVCenter)
			elif self.type() == self.DateType:
				if self.validate(value):
					super().setData(role, value)
				self.setTextAlignment(Qt.AlignCenter)
			elif self.type() == self.DurationType:
				self.setData(Qt.EditRole, value)
				self.setTextAlignment(Qt.AlignCenter)
		elif role == Qt.EditRole:
			if self.type() == self.DurationType:
				if self.validate(value):
					self.editData = value
					date = QDateTime.fromString(value, Qt.ISODate)
					displayString = durationString(date, QDateTime.currentDateTime())
					super().setData(Qt.DisplayRole, displayString)
			else:
				if self.validate(value):
					super().setData(role, value)
		else:
			super().setData(role, value)

	def data(self, role):
		if role == Qt.EditRole:
			if self.type() == self.DurationType:
				return self.editData
		return super().data(role)

	def validate(self, value):
		if self.type() == self.IntType:
			try:
				int(value)
				return True
			except ValueError:
				return False
		elif self.type() in [self.DateType, self.DurationType]:
			date = QDateTime.fromString(value, Qt.ISODate)
			return date.isValid()
		return True

	def __lt__(self, other):
		if self.type() == self.IntType:
			try:
				return int(self.text()) < int(other.text())
			except:
				pass # Do regular string sort. This should not happen.
		elif self.type() == self.DurationType:
			return self.data(Qt.EditRole) < other.data(Qt.EditRole)
		return super().__lt__(other)

class BacklogList(QTableWidget):
	COLUMNS = [
		{'name': 'Priority', 'data_field': 'priority', 'type': BacklogListItem.IntType},
		{'name': 'Game', 'data_field': 'game', 'type': BacklogListItem.StringType},
		{'name': 'Status', 'data_field': 'status', 'type': BacklogListItem.StringType},
		{'name': 'Started', 'data_field': 'started', 'type': BacklogListItem.DateType},
		{'name': 'Last Played', 'data_field': 'last_played', 'type': BacklogListItem.DurationType}
	]

	def __init__(self, parent):
		super().__init__(parent)
		self.setItemPrototype(BacklogListItem('', BacklogListItem.StringType))
		self.setSelectionBehavior(QAbstractItemView.SelectRows)
		self.setSortingEnabled(True)
		self.setColumnCount(len(self.COLUMNS))
		self.setHorizontalHeaderLabels([column['name'] for column in self.COLUMNS])
		self.verticalHeader().hide()
		self.sortItems(1, Qt.AscendingOrder) # Sort by name as secondary criteria.
		self.sortItems(0, Qt.DescendingOrder) # Sort by priority initially.

	def load(self, file):
		# Load saved entries.
		entries = []
		if os.path.exists(file):
			try:
				with open(file, 'r') as f:
					entries = json.load(f)
			except (IOError, ValueError) as e:
				qFatal('Unable to open database file: ' + str(e))

		# Populate table data with entries.
		for entry in entries:
			newRow = self.rowCount()
			self.insertRow(newRow)
			for column, fields in enumerate(self.COLUMNS):
				field = fields['data_field']
				item = BacklogListItem(str(entry[field]), fields['type'])
				self.setItem(newRow, column, item)
				newRow = item.row()

		# Sort the list and adjust column widths.
		self.sort()
		self.resizeColumnsToContents()
		for i, w in enumerate([20, 140, 200, 130, 130]):
			if self.columnWidth(i) < w:
				self.setColumnWidth(i, w)

	def save(self, file):
		# Construct a JSON collection from table data.
		entries = []
		for row in range(self.rowCount()):
			entry = {}
			try:
				for column, fields in enumerate(self.COLUMNS):
					field = fields['data_field']
					type = fields['type']
					item = self.item(row, column)
					value = str(item.data(Qt.EditRole))
					entry[field] = value
				entries.append(entry)
			except AttributeError:
				pass # Do not save newly added rows that haven't been modified.

		# Write database file.
		with open(file, 'w') as f:
			json.dump(entries, f, sort_keys=True, indent=4, separators=(',', ': '))

	def hasSelection(self):
		return bool(self.selectedRanges())

	def collectSelections(self):
		selections = []
		for selectionRange in self.selectedRanges():
			for row in range(selectionRange.topRow(), selectionRange.bottomRow() + 1):
				selections.append(row)
		return selections

	def add(self):
		selections = self.collectSelections()
		row = 0
		if len(selections) > 0: # Useless due to sorting.
			row = selections[0]
		self.insertRow(row)

		# Populate the newly added row with a few template values.
		currentDate = currentDateTimeString()
		for column, value in zip([0, 2, 3, 4], [0, '', currentDate, currentDate]):
			type = self.COLUMNS[column]['type']
			item = BacklogListItem(str(value), type)
			self.setItem(row, column, item)
			row = item.row()

		# Highlight the newly added item.
		self.scrollToItem(item, QAbstractItemView.PositionAtCenter)
		self.clearSelection()
		self.setRangeSelected(QTableWidgetSelectionRange(row, 0, row, self.columnCount() - 1), True)

	def bump(self):
		selections = self.collectSelections()
		items = [self.item(row, 4) for row in selections]
		self.clearSelection()
		for item in items:
			now = currentDateTimeString()
			item.setText(now)
		for item in items: # Build new selection after sorting.
			row = item.row()
			self.setRangeSelected(QTableWidgetSelectionRange(row, 0, row, self.columnCount() - 1), True)
		self.sort()

	def delete(self):
		selections = self.collectSelections()
		for row in sorted(selections, reverse=True):
			self.removeRow(row)
	
	def modifyPriority(self, f):
		selections = self.collectSelections()
		items = [self.item(row, 0) for row in selections]
		self.clearSelection()
		for item in items:
			newPriority = str(f(int(item.text())))
			item.setText(newPriority)
		for item in items: # Build new selection after sorting.
			row = item.row()
			self.setRangeSelected(QTableWidgetSelectionRange(row, 0, row, self.columnCount() - 1), True)
		self.sort()

	def keyPressEvent(self, event):
		key = event.key()
		modifiers = event.modifiers()
		if modifiers != Qt.ControlModifier and key not in [Qt.Key_Insert, Qt.Key_Delete, Qt.Key_Space, Qt.Key_PageUp, Qt.Key_PageDown]:
			super().keyPressEvent(event)

	def keyReleaseEvent(self, event):
		key = event.key()
		modifiers = event.modifiers()
		# Handles the shortcuts for the context menu items.
		if modifiers == Qt.ControlModifier:
			if key == Qt.Key_Insert:
				self.add()
			elif key == Qt.Key_Delete:
				self.delete()
			elif key == Qt.Key_Space:
				self.bump()
			elif key == Qt.Key_PageUp:
				self.modifyPriority(lambda x: x + 1)
			elif key == Qt.Key_PageDown:
				self.modifyPriority(lambda x: x - 1)
			else:
				super().keyReleaseEvent(event)
		else:
			super().keyReleaseEvent(event)

	def sort(self):
		sortSection = self.horizontalHeader().sortIndicatorSection()
		sortOrder = self.horizontalHeader().sortIndicatorOrder()
		self.sortItems(1, Qt.AscendingOrder) # Sort by name as a second criteria.
		self.sortItems(sortSection, sortOrder)

	def contextMenuEvent(self, event):
		menu = BacklogMenu(self)
		menu.showMenu(event.pos())

class MainWindow(QMainWindow):
	def __init__(self):
		super().__init__()
		self.setWindowTitle('Backlogger')
		self.resize(680, 360)
		self.list = BacklogList(self)
		self.list.load(DATABASE_FILE)
		self.list.show()
		self.setCentralWidget(self.list)
		aboutAction = QAction('About', self)
		self.menuBar().addAction(aboutAction)
		aboutAction.triggered.connect(self.showAboutWindow)
		self.show()

	def closeEvent(self, event):
		self.list.save(DATABASE_FILE)
		event.accept()

	def showAboutWindow(self, state):
		QMessageBox.about(self, 'About', 'Backlogger v1.0\n\nThis program uses the PyQt5 and Qt libraries. The licenses for them can be found in the licenses subdirectory.')

if __name__ == '__main__':
	app = QApplication(sys.argv)
	main = MainWindow()
	sys.exit(app.exec_())

