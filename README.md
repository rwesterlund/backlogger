# Backlogger

Version 1.0

## Introduction

Simple tool for keeping track of games in your backlog.

This program was made to keep track of your games backlog in a quick fashion. For that
purpose the program is designed as a minimal spreadsheet where you can quickly add a row
for each game and keep track of things like where you last left off, when you started playing
it and when you last played it. To manage this list a set of keyboard shortcuts as well as
a context menu is provided. It is also possible prioritize games so they get sorted above
everything else, which should come in handy for keeping track of what is played right now
and what should be played next.

## Requirements

This program has been written for Python 3 and requires [PyQt5](http://pyqt.sourceforge.net/)
to run.

## Installation

The application can be placed anywhere as long as it can be run from a working directory
with write permissions. It is a good idea to set up a virtual environment and install
PyQt5 into it using `pip`. On Windows some extra tweaking might be necessary such as copying
the `python3.dll` into the `Scripts` folder of the virtual environment.

## Usage

The program can be run by running `python backlogger.pyw`.
If you have set up file associations for python files you can also run the
script using that method. A `db` file will be created in the working directory which
the program uses to save data between session.

## Known Issues

  * Modifying an element that would cause table to be sorted differently may cause the current selection to point at something else afterwards.
